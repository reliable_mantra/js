# Project setup

You can use the internal js server by running:

```
npm run start
```

the other way is to set up an html server to serve the project but you should set the root folder to **/dist/** instead of the project root witch contains the dist folder.

# Comments

```js
// Single line

/*
    Multi line
*/
```

# Output

```js
document.getElementById("demo").innerHTML = "Hello" + " " + "World!";
document.write("Hello" + " " + "World!");
window.alert("Hello" + " " + "World!");
console.log("Hello" + " " + "World!");
```

# Execution

### Hoisting

 You can use a function before declaration but only inside one document, that is because while parsing parser will first get all functions.

When using an included function from a different document the function declaration (inclusion of the document with the function declaration) has to precede the function usage.

Variables always have to be declared before being used.

```js
myFunction();

function myFunction() {
    console.log('test');
}
```

### Modules

* Important aspect of any robust application's architecture
* Keep the units of code for a project both cleanly separated and organized
* Encapsulate some data into privacy and expose other data publicly.

# Constants

ES2015 intoduced two important new JavaScript keywords: let and const. Variables defined with const behave like let variables, except they cannot be reassigned: 

```js
const PI = 3.141592653589793;
```

# Variables

```js
var a = 5;
```

### Global Scope

```js
var carName = "Volvo";

// code here can use carName

function myFunction() {

    // code here can also use carName

}
```

### Function Scope

```js
var carName = "Volvo";
function myFunction() {
    console.log(carName); // Volvo
    carName = "BMW";
}
myFunction();

console.log(carName); // BMW

var carName = "Volvo";
function myNewFunction(carNameParam) {
    console.log(carName); // Volvo
    console.log(carNameParam); // Volvo
    carNameParam = "BMW";
}
myNewFunction(carName);

console.log(carName); // Volvo
```

## let

ES2015 introduced variables declared with the let keyword.

#### Hoisting

While variables defined with var are hoisted to the top, variables defined with let are not.

```js
// you CAN use carName here
var carName;

// you can NOT use carName here
let carName;
```

#### Global Scope

Global variables defined with the let keyword do not belong to the window object.

```js
var carName = "Volvo";
// code here can use window.carName

let carName = "Volvo";
// code here can not use window.carName
```

#### Block Scope

Variables declared with the let keyword can have Block Scope. Variables declared inside a block {} can not be accessed from outside the block:

```js
var x = 1;
if (true) {
    var x = 5;
}
console.log(x); // 5

var y = 1;
if (true) {
    let y = 5;
}
console.log(y); // 1
```

#### Loop Scope

```js
var i = 5;
for (var i = 0; i < 10; i++) {
    // some statements
}
// Here i is 10

let i = 5;
for (let i = 0; i < 10; i++) {
    // some statements
}
// Here i is 5
```

#### Redeclaring

Redeclaring a var variable with let, in the same scope, or in the same block, is not allowed.

```js
var x = 2;       // Allowed
let x = 3;       // Not allowed

{
    var x = 4;   // Allowed
    let x = 5    // Not allowed
}
      
```

Redeclaring a let variable with let, in the same scope, or in the same block, is not allowed

```js
let x = 2;       // Allowed
let x = 3;       // Not allowed

{
    let x = 4;   // Allowed
    let x = 5;   // Not allowed
}
```

Redeclaring a let variable with var, in the same scope, or in the same block, is not allowed.

```js
let x = 2;       // Allowed
var x = 3;       // Not allowed

{
    let x = 4;   // Allowed
    var x = 5;   // Not allowed
}
    
```

# Data Types

### Primitives

```js
var x;
var y;

// String
x = "Hello world!";

// Integer
x = 5985;

// Floats
x = 10.365;

// Boolean
x = true;
y = false;

// NULL
x = null;

// Undefined
x = undefined;
```

### Objects

```js
// Array
var cars = ["Volvo","BMW","Toyota"];

// Object
var person = {firstName:"John", lastName:"Doe", age:50, eyeColor:"blue"};

// Date
var d = new Date();

// Function
console.log((function(){}).constructor === Function); // true
```

### typeof

```js
typeof "John"                   // Returns "string"
typeof 3.14                     // Returns "number"
typeof true                     // Returns "boolean"
typeof false                    // Returns "boolean"
typeof x                        // Returns "undefined" (if x has no value)
typeof {name:'John', age:34}    // Returns "object"
typeof [1,2,3,4]                // Returns "object" (not "array", see note below)
typeof null                     // Returns "object"
typeof function myFunc(){}      // Returns "function"
```

### Primitives vs Objects

Primitive variables contain the value and object variables contain only the reference to the object. This is why obj2.age is 30, because obj2 is a reference pointing to obj1 which has the age value of 30 since we changed it. 

```js
var a = 23;
var b = a;
a = 46;
console.log(a); // 46
console.log(b); // 23

var obj1 = {
    name: 'John',
    age: 26
}
var obj2 = obj1;
obj1.age = 30;
console.log(obj1.age); // 30
console.log(obj2.age); // 30
```

When we pass a primitive into a function a simple copy is created so you can change it and it will not affect the variable outside, but if you pass an object it's not really the object that we pass but the reference to the object so when we make a change to the passed object it is reflected on the object outside the function. 

```js
var age = 27;
var obj = {
    name: 'Jonas',
    city: 'Lisbon'
};

function change(a, b) {
    a = 30;
    b.city = 'San Francisco';
}

change(age, obj);

console.log(age);       // 27
console.log(obj.city);  // San Francisco
```

# Operators

### Arithmetic Operators

* "+" (Addition)
* "-" (Subtraction)
* "*" (Multiplication)
* "/" (Division)
* "%" (Modulus - Division Remainder)
* "++" (Increment)
* "--" (Decrement)

### String Operators

* "+" (Concatenation)
* "+=" (Concatenation assignment)

### Assignment Operators

* x = y
* x += y
* x -= y
* x *= y
* x /= y
* x %= y

### Comparison Operators

* "==" (Equal)
* "===" (Identical)
* "!=" (Not equal)
* "!==" (Not identical)
* ">" (Greater than)
* "<" (Less than)
* ">=" (Greater than or equal to)
* "<=" (Less than or equal to)

### Logical Operators

* "&&" (And)
* "||" (Or)
* "!" (Not)

### Type Operators

* typeof
* instanceof

# Conditions

### If...Else

```js
var greeting;
var time = new Date().getHours();

if (time < 10) {
    greeting = "Good morning";
} else if (time < 20) {
    greeting = "Good day";
} else {
    greeting = "Good evening";
}

// Ternary
console.log( (time < 10) ? "Good morning" : "Good day" );
```

### Switch

```js
var text;
var favcolor = "red";

switch (favcolor) {
    case "red":
        text = "Your favorite color is red!";
        break;
    case "blue":
        text = "Your favorite color is blue!";
        break;
    case "green":
        text = "Your favorite color is green!";
        break;
    default:
        text = "Your favorite color is neither red, blue, nor green!";
}
```

# Loops

### For

```js
for (var i=0; i<=3; i++) { 
    text += "The number is " + i;
}
```

### While

```js
var i = 1;
while (i <= 3) {
    text += "The number is " + i;
    i++;
}
```

### Do...While

```js
var i = 1;
do{
    text += "The number is " + i;
    $i++;
}
while(i <= 3);
```

### For...in

```js
var person = {fname:"John", lname:"Doe", age:25}; 

var x;
for (x in person) {
    text += person[x] + " ";
}
```

# Functions

* A function is an instance of the Object type
* A function behaves like any other object

JavaScript allows us to call any function without specifying all of the arguments, what Javascript does in this case is that it simply assigns undefined to the not passed arguments. 

```js
function sum(x, y) {
    x = (x === undefined) ? 5 : x;
    y = (y === undefined) ? 5 : y

    var z = x + y;
    return z;
}

sum();
```

In ES6 we have default parameters and they behave as expected.

```js
function sum(x=5, y=5) {
    var z = x + y;
    return z;
}

sum();
```

### Anonymous functions

* We can store functions in a variable
* We can pass a function as an argument to another function
* We can return a function from a function

```js
var x = function (a, b) {return a * b};
var z = x(4, 3);
```

### Self-Invoking Functions

```js
(function () {
    var x = "Hello!!";      // I will invoke myself
})();
```

### Callback Functions

```js
var years = [1990, 1965, 1937, 2005, 1998];

function arrayCalc(arr, fn) {
    var arrRes = [];

    for (var i = 0; i < arr.length; i++) {
        arrRes.push(fn(arr[i]));
    }

    return arrRes;
}

function calculateAge(el) {
    return 2016 - el;
}

function isFullAge(el) {
    return el >= 18;
}

var ages = arrayCalc(years, calculateAge);
var fullAges = arrayCalc(ages, isFullAge);

console.log(ages); // [26, 51, 79, 11, 18]
console.log(fullAges); // [true, true, true, false, true]
```

### Functions returning functions

```js
function interviewQuestion(job) {
    if (job === 'designer') {
        return function(name) {
            console.log(name + ', can you please explain what UX design is?');
        }
    } else if (job === 'teacher') {
        return function(name) {
            console.log('What subject do you teach, ' + name + '?');
        }
    } else {
        return function(name) {
            console.log('Hello ' + name + ', what do you do?');
        }
    }
}

var teacherQuestion = interviewQuestion('teacher');

teacherQuestion('John'); // What subject do you teach, John?
```

### Closures

An inner function has always access to the variables and parameters of its outer function, even after the outer function has returned. 

```js
function retirement(retirementAge) {
    var a = ' years left until retirement.';
    return function(yearOfBirth) {
        var age = 2016 - yearOfBirth;
        console.log((retirementAge - age) + a);
    }
}

var retirementUS = retirement(66);
retirementUS(1990); // 40 years left until retirement.
```

### Bind, call and apply function object methods

```js
var john = {
    name: 'John',
    age: 26,
    job: 'teacher',
    presentation: function(style, timeOfDay) {
        if (style === 'formal') {
            console.log('Good ' + timeOfDay + ', Ladies and gentlemen! I\'m ' +  this.name + ', I\'m a ' + this.job + ' and I\'m ' + this.age + ' years old.');
        } else if (style === 'friendly') {
            console.log('Hey! What\'s up? I\'m ' +  this.name + ', I\'m a ' + this.job + ' and I\'m ' + this.age + ' years old. Have a nice ' + timeOfDay + '.');
        }
    }
};

var emily = {
    name: 'Emily',
    age: 35,
    job: 'designer'
};

john.presentation('formal', 'morning');

john.presentation.call(emily, 'friendly', 'afternoon');

//john.presentation.apply(emily, ['friendly', 'afternoon']);

var johnFriendly = john.presentation.bind(john, 'friendly');

johnFriendly('morning');
johnFriendly('night');

var emilyFormal = john.presentation.bind(emily, 'formal');
emilyFormal('afternoon');
```

### "arguments" keyword

Suppose we want to create a function that accepts an arbitrary number of parameters, then we simply don't define any parameters for the function and then just use the arguments keyword. Arguments variable is very similar to the this variable in a seance that it is a variable tha each execution context gets access to. Arguments variable is not an array but has an array like structure, still in order to use it like an array you have to transform it to an array. 

```js
function isFullAge() {
    console.log(arguments);
    var argsArray = Array.prototype.slice.call(arguments);
}
```

### (ES6) Arrow Functions

```js
const years = [1990, 1965, 1982, 1937];

// ES5
var ages4 = years.map(function(el) {
   return 2016 - el;
});

// ES6
let ages6 = years.map(el => 2016 - el);

ages6 = years.map((el, index) => `Age element ${index + 1}: ${2016 - el}.`);

ages6 = year.map((el, index) => {
    const now = new Date().getFullYear();
    const age = now - el;
    return `Age element ${index + 1}: ${age}.`;
});
```

The biggest advantage of using arrow functions is that they share the surrounding this keyword. So, this means that, unlike normal functions, arrow functions don't get their own this keyword. 

```js
// ES5
var box5 = {
    color: 'green',
    position: 1,
    clickMe: function() {
        var _self = this;

        document.querySelector('.green').addEventListener('click', function() {
            var str = 'This is box number ' + _self.position + ' and it is ' + _self.color;
        });
    }
};

// ES6
var box6 = {
    color: 'green',
    position: 1,
    clickMe: function() {
        document.querySelector('.green').addEventListener('click', () => {
            var str = `This is box number ${this.position} and it is ${this.color}`;
        });
    }
};
```

# this

Pseudo-variable this references the window object when called in the global scope or within a regular function definition. When called inside a method it references the calling object. 

```js
console.log(this);                // Window.{}

function myFunction() {
    console.log(this);            // Window.{}
}
myFunction();

var john = {
    name: 'John',
    calculateAge: function () {
        console.log(this);          // Object{name: "John"}

        function innerFunction() {
            console.log(this);      // Window.{}
        }
        innerFunction();
    }
}
john.calculateAge();
```

# OOP

JavaScript is a prototype based language. Instead of classes it has Constructors which contain prototypes.

* Every JavaScript object has a **prototype property**, which makes inheritance possible in JavaScript
* The prototype property of an object is where we put methods and properties that we want **other objects to inherit**
* The Constructor's prototype property is **NOT** the prototype of the Constructor itself, it's the prototype of **ALL** instances that are created trough it 
* When a certain method (or property) is called, the search starts in the object itself, and if cannot be found, the search moves on to the objects's prototype. This continues until the method is found: **prototype chain**

```js
// Function constructor
var Person = function(name, yearOfBirth, job) {
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;

    // Each of the objects created using this constructor
    // will have the calculateAge() method attached to them,
    // in this case it's just one function but imagine you havd 20 large functions...
    // then this wouldn't be efficient because we would have
    // 3 copies of the same thing for our 3 objects.
    // That is why we use inheritance instead.
    // this.calculateAge = function() {
    //     console.log(2016 - this.yearOfBirth);
    // }
}

// This is the way we properly set up inheritance.
Person.prototype.calculateAge = function() {
    console.log(2016 - this.yearOfBirth);
};

var john = new Person('John', 1990, 'teacher');
john.calculateAge();  // 26

var jane = new Person('Jane', 1969, 'designer');
var mark = new Person('Mark', 1948, 'retired');

// Object.create
var personPrototype = {
    calculateAge: function() {
        console.log(2016 - this.yearOfBirth);
    }
};

var john = Object.create(personPrototype);
john.name = 'John';
john.yearOfBirth = '1990';
john.job = 'teacher';
```

## (ES6) Classes

In ES5 object blueprints are called **function constructors** and we added methods to their prototype properties in order to make all instances created trough a function constructor inherit these methods. 

```js
// ES5
var Person5 = function(name, yearOfBirth, job) {
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;
};

Person5.prototype.calculateAge = function() {
    var age = new Date().getFullYear() - this.yearOfBirth;
    console.log(age);
};

var john5 = new Person5('John', 1990, 'teacher');

var Athlete5 = function(name, yearOfBirth, job, olympicGames, medals) {
    Person5.call(this, name, yearOfBirth, job);
    this.olypmicGames = olympicGames;
    this.medals = medals;
};

Athlete5.prototype = Object.create(Person5.prototype);
Athlete5.prototype.wonMedal = function() {
    this.medals++;
    console.log(this.medals);
};

var johnAthlete5 = new Athlete5('John', 1990, 'swimmer', 3, 10);

johnAthlete5.calculateAge();
johnAthlete5.wonMedal();

// ES6
class Person6 {
    constructor (name, yearOfBirth, job) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.job = job;
    }

    calculateAge() {
        var age = new Date().getFullYear() - this.yearOfBirth;
        console.log(age);
    }

    static greeting() {
        console.log('Hey there!');
    }
}

const john6 = new Person6('John', 1990, 'teacher');

Person6.greeting();

class Athlete6 extends Person6 {
    constructor(name, yearOfBirth, job, olympicGames, medals) {
        super(name, yearOfBirth, job);
        this.olympicGames = olympicGames;
        this.medals = medals;
    }

    wonMedal() {
        this.medals++;
        console.log(this.medals);
    }
}

const johnAthlete6 = new Athlete6('John', 1990, 'swimmer', 3, 10);

johnAthlete6.calculateAge();
johnAthlete6.wonMedal();
```

# Try and catch

```js
try {

} catch(error) {
    console.log(error);
}
```

# Modules

```js
// Default exports
export default 'I am an exported string.';

import str from './models/Search';

// Named exports
export const add = (a, b) => a + b;
export const multiply = (a, b) => a * b;
export const ID = 23;

import { add, multiply as m, ID } from './views/searchVIew';
console.log(`Using imported functions! ${add(ID, 2)} and ${m(3, 5)}. ${str}`);

// OR

import * as searchView from './views/searchView';
console.log(`Using imported functions! ${searchView.add(searchView.ID, 2)} and ${searchView.multiply(3, 5)}. ${str}`);
```

# DOM

### HTMLCollection, NodeList and array of objects

```js
var parentDiv = document.querySelector('.test-container');
var nodeListDivs = document.querySelectorAll('.divy');
var htmlCollectionDivs = document.getElementsByClassName('divy');

console.log(nodeListDivs.length); //=> 4
console.log(htmlCollectionDivs.length); //=> 4

//append new child to container
var newDiv = document.createElement('div');
newDiv.className = 'divy';
parentDiv.appendChild(newDiv);

console.log(nodeListDivs.length); //=> 4
console.log(htmlCollectionDivs.length); //=> 5
```

# ES6

### Destructuring

```js
// ES5
var john = ['John', 26];
var name = john[0];
var age = john[1];


// ES6
const [name, age] = ['John', 26];
console.log(name);
console.log(age);

const obj = {
    firstName: 'John',
    lastName: 'Smith'
};
const {firstName, lastName} = obj;
const {firstName: a, lastName: b} = obj;

function calcAgeRetirement(year) {
    const age = new Date().getFullYear() - year;
    return [age, 65 - age];
}
const [age2, retirement] = calcAgeRetirement(1990);
```

### Arrays

Create array from node list without hacks.

```js
const boxes = document.querySelectorAll('.box');

//ES5
var boxesArr5 = Array.prototype.slice.call(boxes);
boxesArr5.forEach(function(cur) {
    cur.style.backgroundColor = 'dodgerblue';
});

//ES6
const boxesArr6 = Array.from(boxes);
Array.from(boxes).forEach(cur => cur.style.backgroundColor = 'dodgerblue');
```

New for loop for arrays.

```js
//ES5
for(var i = 0; i < boxesArr5.length; i++) {

    if(boxesArr5[i].className === 'box blue') {
        continue;
    }

    boxesArr5[i].textContent = 'I changed to blue!';
}


//ES6
for (const cur of boxesArr6) {
    if (cur.className.includes('blue')) {
        continue;
    }
    cur.textContent = 'I changed to blue!';
}
```

### New operator (Spread operator)

Used to spread an array when passed as a parameter.

```js
function addFourAges (a, b, c, d) {
    return a + b + c + d;
}

var sum1 = addFourAges(18, 30, 12, 21);

//ES5
var ages = [18, 30, 12, 21];
var sum2 = addFourAges.apply(null, ages);

//ES6
const sum3 = addFourAges(...ages);

const familySmith = ['John', 'Jane', 'Mark'];
const familyMiller = ['Mary', 'Bob', 'Ann'];
const bigFamily = [...familySmith, 'Lily', ...familyMiller];

const h = document.querySelector('h1');
const boxes = document.querySelectorAll('.box');
const all = [h, ...boxes];

Array.from(all).forEach(cur => cur.style.color = 'purple');
```

### New operator (Rest parameters operator)

The opposite of the spread operator and is used to create an array from several parameters.

```js
//ES5
function isFullAge5() {
    //console.log(arguments);
    var argsArr = Array.prototype.slice.call(arguments);

    argsArr.forEach(function(cur) {
        console.log((2016 - cur) >= 18);
    })
}

//ES6
function isFullAge6(...years) {
    years.forEach(cur => console.log( (2016 - cur) >= 18));
}
```

### Maps (Something like associative arrays)

Maps are a new data structure, and in case if you are wandering this doesn't have anything to do with maps (like Google maps or something). Very common use of objects in javascript is to use them as hash maps, which simply means that we map string keys to arbitrary values and we had to use objects for that up until ES6. Now we something better for this use case and that's maps. A map i a new key value data structure and one of the differences is that in maps we can use anything for the keys, so in objects we are limited to strings but in maps we can use any kind of primitive value like numbers, strings or booleans and we can even use functions or objects as keys. 

```js
const question = new Map();

// SET
question.set('question', 'What is the official name of the latest major JavaScript version?');
question.set(1, 'ES5');
question.set(2, 'ES6');
question.set(3, 'ES2015');
question.set(4, 'ES7');
question.set('correct', 3);
question.set(true, 'Correct answer :D');
question.set(false, 'Wrong, please try again!');

// GET
console.log(question.get('question'));
console.log(question.size);

// HAS and DELETE
if(question.has(4)) {
    question.delete(4);
}

// CLEAR
question.clear();

// FOREACH
question.forEach((value, key) => console.log(`This is ${key}, and it's set to ${value}`));

// FOR
for (let [key, value] of question.entries()) {
    if (typeof(key) === 'number') {
        console.log(`Answer ${key}: ${value}`);
    }
}
```

# Asynchronous

* Allow asynchronous functions to run in the "background"
* We pass in callbacks that run once the function has finished its work
* Move on immediately: Non-blocking!

JavaScript runtime has WEB APIS and MESSAGE QUEUE outside its EXECUTION STACK and they both work together asynchronously: setTimeout(), DOM events, XMLHttpRequest(), geolocation, local storage... 

```js
// Synchronous
const second = () => {
    console.log('How are you doing?');
};

const first = () => {
    console.log('Hey There!'); // 1
    second();                  // 2
    console.log('The end');    // 3
};

first();


// Asynchronous
const second = => {
    setTimeout(() => {
        console.log('Async Hey There!');
    }, 2000);
};

const first = () => {
    console.log('Hey there!'); // 1
    second();                  // 3
    console.log('The end');    // 2
};

first();
```

### Callback hell

Callback hell simply means that we have callback inside of callback inside of callback and that sometimes gets really unmanageable. 

```js
function getRecipe() {
    setTimeout(() => {
        const recipeID = [523, 883, 432, 974];
        console.log(recipeID);

        setTimeout(id => {
            const recipe = {title: 'Fresh tomato pasta', publisher: 'Jonas'};
            console.log(`${id}: ${recipe.title}`);

            setTimeout(publisher => {
                const recipe2 = {title: 'Italian Pizza', publisher: 'Jonas'};
                console.log(recipe);
            }, 1500, recipe.publisher);

        }, 1500, recipeID[2]);

    }, 1500);
}
getRecipe();
```

### (ES6)Promises

* Object that keeps track about whether a certain event has happened already or not
* Determines what happens after the event has happened
* Implements the concept of a future value that we're expecting

Its like us saying "Hey, get me some data from the server in the background" and the promise the promises us to get that data so we can handle it in the future. 

Promise states:

* PENDING (Before the event has happened)
* SETTLED/RESOLVED (After the event has happened)
* FULFILLED (When the promise was successful which means that the result is available)
* REJECTED (If there was an error)

```js
const getIDs = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve([523, 883, 432, 974]);
    }, 1500);
});

const getRecipe = recID => {
    return new Promise((resolve, reject) => {
        setTimeout(ID => {
            const recipe = {title: 'Fresh tomato pasta', publisher: 'Jonas'};
            resolve(`${ID}: ${recipe.title}`);
        }, 1500, recID);
    });
};

const getRelated = publisher => {
    return new Promise((resolve, reject) => {
        setTimeout(pub => {
            const recipe = {title: 'Italian Pizza', publisher: 'Jonas'};
            resolve(`${pub}: ${recipe.title}`);
        }, 1500, publisher);
    });
};

getIDs
.then(IDs => {
    console.log(IDs);
    return getRecipe(IDs[2]);
})
.then(recipe => {
    console.log(recipe);
    return getRelated('Jonas Schmedtmann');
})
.then(recipe => {
    console.log(recipe);
})
.catch(error => {
    console.log('Error!!');
});
```

### (ES8) async and await

Used for consuming promises. Await expression can only be used inside an async function and the async function runs in the background.

```js
const getIDs = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve([523, 883, 432, 974]);
    }, 1500);
});

const getRecipe = recID => {
    return new Promise((resolve, reject) => {
        setTimeout(ID => {
            const recipe = {title: 'Fresh tomato pasta', publisher: 'Jonas'};
            resolve(`${ID}: ${recipe.title}`);
        }, 1500, recID);
    });
};

const getRelated = publisher => {
    return new Promise((resolve, reject) => {
        setTimeout(pub => {
            const recipe = {title: 'Italian Pizza', publisher: 'Jonas'};
            resolve(`${pub}: ${recipe.title}`);
        }, 1500, publisher);
    });
};

async function getRecipesAW() {
    const IDs = await getIDs;
    console.log(IDs);
    const recipe = await getRecipe(IDs[2]);
    console.log(recipe);
    const related = await getRelated('Jonas Schmedtmann');
    console.log(related);

    return recipe;
}
getRecipesAW().then(result => console.log(`${result} is the best ever!`));
```

# AJAX

```js
// Then, catch
function getWeather(woeid) {
    fetch(`https://cors-anywhere.herokuapp.com/https://www.metaweather.com/api/location/${woeid}/`)
        .then(result => {
            // console.log(result);
            return result.json();
        })
        .then(data => {
            // console.log(data);
            const today = data.consolidated_weather[0];
            console.log(`Temperatures in ${data.title} stay between ${today.min_temp} and ${today.max_temp}`);
        })
        .catch(error => {
            console.log(error);
        });
}
getWeather(2487956);
getWeather(44418);

// async, await
async function getWeatherAW(woeid) {
    try {
        const result = await fetch(`https://cors-anywhere.herokuapp.com/https://www.metaweather.com/api/location/${woeid}/`);
        const data = await result.json();
        const today = data.consolidated_weather[0];
        console.log(`Temperatures in ${data.title} stay between ${today.min_temp} and ${today.max_temp}`);
        return data;
    } catch(error) {
        console.log(error);
    }
}
getWeatherAW(2487956);
let dataLondon;
getWeatherAW(44418).then(data => {
    dataLondon = data;
    console.log(dataLondon);
});
```

# Local Storage

localStorage api allows us to very simply keep key value pairs right in the browser. This data will stay intact even after page reloads.

```js
localStorage.setItem('key', 'value');
localStorage.getItem('key');
localStorage.removeItem('key');

localStorage.setItem('key', JSON.stringify(object));
```
